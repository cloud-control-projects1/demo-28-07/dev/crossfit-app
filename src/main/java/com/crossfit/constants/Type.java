package com.crossfit.constants;

public enum Type {
    ALL, AEROBIC, MUSCLE_WEIGHT, MUSCLE_BODY_WEIGHT, STRETCHING
}
