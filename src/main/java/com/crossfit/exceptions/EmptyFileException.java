package com.crossfit.exceptions;

public class EmptyFileException extends Exception {
    public EmptyFileException(String message) {
        super(message);
    }
}
