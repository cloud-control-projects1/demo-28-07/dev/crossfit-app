package com.crossfit.dto;

import com.crossfit.model.Exercise;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CrossfitExercise {

    private Exercise exercise;
    private int reps;
}
