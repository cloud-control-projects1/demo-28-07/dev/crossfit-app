package com.crossfit.services;

import com.crossfit.model.Exercise;
import com.crossfit.model.WorkoutExerciseType;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class RandomizerForExercises {

    public static Stream<Exercise> randomizeExercises(int exercisesCount, List<Exercise> exercises) {
        return new Random()
                .ints(exercisesCount, 0, exercises.size())
                .mapToObj(exercises::get);
    }

    public static WorkoutExerciseType randomizeWorkoutExerciseType(List<WorkoutExerciseType> exerciseTypes) {
        return exerciseTypes
                .get(new Random()
                        .nextInt(exerciseTypes.size()));
    }
}
