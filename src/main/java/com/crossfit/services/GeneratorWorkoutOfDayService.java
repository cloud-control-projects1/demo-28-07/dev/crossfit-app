package com.crossfit.services;

import com.crossfit.dto.GeneratorDto;
import com.crossfit.dto.WorkoutOfDayView;

public interface GeneratorWorkoutOfDayService {

    WorkoutOfDayView generateWorkout(GeneratorDto generatorDto);
}
