package com.crossfit.services.impl;

import com.crossfit.model.Role;
import com.crossfit.constants.Status;
import com.crossfit.model.User;
import com.crossfit.exceptions.UserAlreadyExistException;
import com.crossfit.repo.RoleRepository;
import com.crossfit.repo.UserRepository;
import com.crossfit.security.jwt.JwtTokenProvider;
import com.crossfit.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.lang.String.format;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder = bCryptPasswordEncoder();
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    public UserServiceImpl(
            UserRepository userRepository,
            RoleRepository roleRepository,
//            @Lazy BCryptPasswordEncoder passwordEncoder,
            AuthenticationManager authenticationManager,
            JwtTokenProvider jwtTokenProvider
    ) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
//        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostConstruct
    public void initRoles(){
        if(roleRepository.findByName("ROLE_USER")== null){
            Role userRole = new Role();
            userRole.setName("ROLE_USER");
            roleRepository.save(userRole);
        }
        if(roleRepository.findByName("ROLE_ADMIN")== null){
            Role userRole = new Role();
            userRole.setName("ROLE_ADMIN");
            roleRepository.save(userRole);
        }
    }

    @Override
    public String processOAuthPostLogin(Map<String, Object> attr) {
        User client = userRepository.findByUsername((String) attr.get("email"));

        if (client == null) {
            Role roleUser = roleRepository.findByName("ROLE_USER");
            List<Role> userRoles = new ArrayList<>();
            userRoles.add(roleUser);
            User newClient = new User();
            newClient.setRoles(userRoles);
            newClient.setStatus(Status.ACTIVE);
            newClient.setCreatedAt(new Date());
            newClient.setUsername((String) attr.get("email"));
            newClient.setEmail((String) attr.get("email"));
            newClient.setName((String) attr.get("name"));
            newClient.setPic((String) attr.get("picture"));
            newClient.setPassword(passwordEncoder.encode((String) attr.get("sub")));

            client = userRepository.save(newClient);

            log.info("IN register - client form google: {} successfully registered", client);
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(client.getUsername(), (String) attr.get("sub")));
        String token = jwtTokenProvider.createToken(client.getUsername(), client.getRoles());
        return token;
    }

    @Override
    public User register(User user) throws UserAlreadyExistException {
        checkUserExisting(user);
        Role roleUser = roleRepository.findByName("ROLE_USER");
        Role roleAdmin = roleRepository.findByName("ROLE_ADMIN");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);
        userRoles.add(roleAdmin);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setStatus(Status.ACTIVE);
        user.setCreatedAt(new Date());

        User registeredUser = userRepository.save(user);

        log.info("IN register - user: {} successfully registered", registeredUser);

        return registeredUser;
    }

    @Override
    public List<User> getAll() {
        List<User> result = userRepository.findAll();
        log.info("IN getAll - {} users found", result.size());
        return result;
    }

    @Override
    public User findByUsername(String username) {
        User result = userRepository.findByUsername(username);
        log.info("IN findByUsername - user: {} found by username: {}", result, username);
        return result;
    }

    @Override
    public User findById(UUID id) {
        User result = userRepository.findById(id).orElse(null);

        if (result == null) {
            log.warn("IN findById - no user found by id: {}", id);
            return null;
        }

        log.info("IN findById - user: {} found by id: {}", result);
        return result;
    }

    @Override
    public void delete(UUID id) {
        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted");
    }

    private void checkUserExisting(User user) throws UserAlreadyExistException {
        User existingUser = userRepository.findByUsername(user.getUsername());
        if (existingUser != null) {
            log.info("IN register - user: {} already exists", existingUser);
            throw new UserAlreadyExistException(format("IN register - user: %s already exist", user.getUsername()));
        }
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
