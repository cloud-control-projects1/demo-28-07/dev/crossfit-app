package com.crossfit.dto;

import com.crossfit.constants.Format;
import com.crossfit.constants.WodPart;
import com.crossfit.model.WorkoutExerciseType;
import lombok.Data;

@Data
public class WorkoutExerciseTypeDto {

    private String weight;
    private int sets;
    private int reps;
    private int time;
    private Format format;
    private WodPart wodPart;

    public WorkoutExerciseType toWorkoutExerciseType() {
        WorkoutExerciseType workoutExerciseType = new WorkoutExerciseType();
        workoutExerciseType.setWeight(this.weight);
        workoutExerciseType.setSets(this.sets);
        workoutExerciseType.setReps(this.reps);
        workoutExerciseType.setTime(this.time);
        workoutExerciseType.setFormat(this.format);
        workoutExerciseType.setWodPart(this.wodPart);
        return workoutExerciseType;
    }
}
