
package com.crossfit.exceptions;

public class ExerciseNameAbsentException extends Exception {
    public ExerciseNameAbsentException(String message) {
        super(message);
    }
}
