package com.crossfit.controllers;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.model.Exercise;
import com.crossfit.repo.ExerciseRepository;
import com.crossfit.services.ExerciseService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Date;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("ALL")
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ExerciseControllerV1Test {

    private static final String API_URL = "/api/v1/exercise";

    @Mock
    private ExerciseService exerciseService;
    @Mock
    private ExerciseRepository repository;
    @InjectMocks
    private ExerciseControllerV1 exerciseControllerV1;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(exerciseControllerV1).build();
    }

    @Test
    void getExerciseById() throws Exception {
        UUID testId = UUID.randomUUID();
        Mockito.when(exerciseService.getById(testId))
                .thenReturn(createExerciseWithId(testId));
        mockMvc.perform(get(API_URL + "/" + testId))
                .andExpect(status().isOk());
    }

    @Test
    void deleteExercise() throws Exception {
        UUID testId = UUID.randomUUID();
        Mockito.when(exerciseService.delete(testId))
                .thenReturn(createExerciseWithId(testId));
        mockMvc.perform(delete(API_URL + "/" + testId))
                .andExpect(status().isOk());
    }


    @Test
    void newExercise() throws Exception {
        UUID testId = UUID.randomUUID();
        Exercise exercise = createExerciseWithId(testId);
        Mockito
                .when(repository.save(Mockito.any(Exercise.class)))
                .thenReturn(exercise);

        mockMvc.perform(
                        post(API_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(getJsonString(exercise))
                )
                .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
        UUID testId = UUID.randomUUID();
        Exercise exercise = createExerciseWithId(testId);
        Mockito
                .when(exerciseService.getById(testId))
                .thenReturn(exercise);
        String name = "Another";
        exercise.setName(name);
        mockMvc.perform(
                        put(API_URL + "/" + testId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(getJsonString(exercise))
                )
                .andExpect(status().isOk());
    }

    private String getJsonString(Exercise exercise) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(exercise);
    }

    private Exercise createExerciseWithId(UUID id) {
        Exercise exercise = createExercise();
        exercise.setId(id);
        return exercise;
    }

    private Exercise createExercise() {
        Exercise exercise = new Exercise();
        exercise.setName("Test");
        exercise.setDescription("Test");
        exercise.setLevel(Level.AMATEUR);
        exercise.setType(Type.AEROBIC);
        exercise.setMuscleGroup(MuscleGroup.ABS);
        exercise.setUpdatedAt(new Date(System.currentTimeMillis()));
        return exercise;
    }
}
