package com.crossfit.constants;

public enum MuscleGroup {
    FULL_BODY, CHEST, BACK, ARMS, SHOULDERS, ABS, LEGS
}
