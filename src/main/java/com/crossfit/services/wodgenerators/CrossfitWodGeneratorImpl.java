package com.crossfit.services.wodgenerators;

import com.crossfit.constants.Format;
import com.crossfit.constants.Level;
import com.crossfit.constants.WodPart;
import com.crossfit.dto.CrossfitExercise;
import com.crossfit.dto.WorkoutExercise;
import com.crossfit.model.Exercise;
import com.crossfit.model.WorkoutExerciseType;
import com.crossfit.repo.ExerciseRepository;
import com.crossfit.repo.WorkoutExerciseTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static com.crossfit.constants.Constants.*;
import static com.crossfit.services.RandomizerForExercises.randomizeExercises;
import static com.crossfit.services.RandomizerForExercises.randomizeWorkoutExerciseType;

@Component
public class CrossfitWodGeneratorImpl implements CrossfitWodGenerator {

    private final WorkoutExerciseTypeRepository workoutExerciseTypeRepository;
    private final ExerciseRepository exerciseRepository;

    @Autowired
    public CrossfitWodGeneratorImpl(
            WorkoutExerciseTypeRepository workoutExerciseTypeRepository,
            ExerciseRepository exerciseRepository) {
        this.workoutExerciseTypeRepository = workoutExerciseTypeRepository;
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public Set<WorkoutExercise> buildWorkoutExerciseForCrossfit(int metconCrossfitCount, Level level) {
        Set<WorkoutExercise> wod = new HashSet<>();
        for (int i = 0; i < metconCrossfitCount; i++) {
            Set<CrossfitExercise> crossfitExercises = new HashSet<>();
            List<WorkoutExerciseType> exerciseTypes = workoutExerciseTypeRepository
                    .getByWodPartAndFormatForCrossfit(WodPart.WOD);
            WorkoutExerciseType workoutExerciseType = randomizeWorkoutExerciseType(exerciseTypes);
            WorkoutExercise workoutExercise = WorkoutExercise
                    .builder()
                    .workoutExerciseType(workoutExerciseType)
                    .crossfitExercises(crossfitExercises)
                    .build();
            wod.add(workoutExercise);
            createCrossfitExercises(crossfitExercises, workoutExerciseType, level);
        }
        return wod;
    }

    private void createCrossfitExercises(
            Set<CrossfitExercise> crossfitExercises,
            WorkoutExerciseType workoutExerciseType,
            Level level) {
        List<Exercise> wodExercises = getCrossfitWodExercises(level);
        Set<Exercise> randomExercises = randomizeExercises(EXERCISES_COUNT_WOD,
                Objects.requireNonNull(wodExercises)).collect(Collectors.toSet());
        for (Exercise exercise : randomExercises) {
            CrossfitExercise crossfitExercise = CrossfitExercise
                    .builder()
                    .exercise(exercise)
                    .reps(setRandomizeReps(workoutExerciseType.getFormat()))
                    .build();
            crossfitExercises.add(crossfitExercise);
        }
    }

    private List<Exercise> getCrossfitWodExercises(Level level) {
        switch (level) {
            case BEGINNER:
                return exerciseRepository.getByWodPartForBeginnerForCrossfit(WodPart.WOD);
            case AMATEUR:
                return exerciseRepository.getByWodPartForAmateurForCrossfit(WodPart.WOD);
            case PRO:
                return exerciseRepository.getByWodPartAndMuscleGroupForCrossfit(WodPart.WOD);
            default:
                return null;
        }
    }

    private int setRandomizeReps(Format format) {
        switch (format) {
            case AMRAP:
                return AMRAP_REPS_COUNT[new Random().nextInt(AMRAP_REPS_COUNT.length)];
            case AFAP:
                return AFAP_REPS_COUNT[new Random().nextInt(AFAP_REPS_COUNT.length)];
            case EMOM:
                return EMOM_REPS_COUNT[new Random().nextInt(EMOM_REPS_COUNT.length)];
            case CHIPPER:
                return CHIPPER_REPS_COUNT[new Random().nextInt(CHIPPER_REPS_COUNT.length)];
            default:
                return 0;
        }
    }
}
