package com.crossfit.model;

import com.crossfit.constants.Format;
import com.crossfit.constants.WodPart;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "workout_exercise_type")
@Data
public class WorkoutExerciseType extends BaseEntity {

    private String weight;
    private int sets;
    private int reps;
    private int time;
    @Enumerated(EnumType.STRING)
    private Format format;
    @Enumerated(EnumType.STRING)
    private WodPart wodPart;
}
