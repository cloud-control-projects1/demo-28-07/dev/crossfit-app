package com.crossfit.constants;

public enum WorkoutOfDayType {
    GYM, CROSSFIT, FITNESS
}
