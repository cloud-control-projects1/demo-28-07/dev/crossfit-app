package com.crossfit.controllers;

import com.crossfit.dto.GeneratorDto;
import com.crossfit.dto.WorkoutOfDayView;
import com.crossfit.services.GeneratorWorkoutOfDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@PreAuthorize("hasRole('ROLE_USER')")
@RestController
@RequestMapping(value = "api/v1/workoutofday")
public class GeneratorWorkoutOfDayControllerV1 {

    private final GeneratorWorkoutOfDayService generatorWODService;

    @Autowired
    public GeneratorWorkoutOfDayControllerV1(GeneratorWorkoutOfDayService generatorWODService) {
        this.generatorWODService = generatorWODService;
    }

    @PostMapping("/generate")
    public ResponseEntity<Object> searchWorkoutOfDay(@RequestBody GeneratorDto generatorDto) {
        try {
            WorkoutOfDayView workoutOfDayView = generatorWODService.generateWorkout(generatorDto);
            return ResponseEntity.status(HttpStatus.OK).body(workoutOfDayView);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }
}
