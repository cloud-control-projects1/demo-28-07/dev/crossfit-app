package com.crossfit.services.impl;

import com.crossfit.constants.Format;
import com.crossfit.constants.LocaleKeys;
import com.crossfit.constants.WodPart;
import com.crossfit.dto.WorkoutExerciseTypeCsvDto;
import com.crossfit.dto.WorkoutExerciseTypeDto;
import com.crossfit.exceptions.EmptyFileException;
import com.crossfit.exceptions.ExtensionFileException;
import com.crossfit.exceptions.WorkoutExerciseTypeExistException;
import com.crossfit.exceptions.WorkoutExerciseTypeNotFoundException;
import com.crossfit.model.WorkoutExerciseType;
import com.crossfit.repo.WorkoutExerciseTypeRepository;
import com.crossfit.services.WorkoutExerciseTypeService;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;

import static com.crossfit.services.Utils.getStringListValuesFromArray;

@Service
@Slf4j
public class WorkoutExerciseTypeServiceImpl implements WorkoutExerciseTypeService {

    private final WorkoutExerciseTypeRepository workoutExerciseTypeRepository;

    @Autowired
    public WorkoutExerciseTypeServiceImpl(WorkoutExerciseTypeRepository workoutExerciseTypeRepository) {
        this.workoutExerciseTypeRepository = workoutExerciseTypeRepository;
    }

    @Override
    public WorkoutExerciseType getByID(UUID id) throws WorkoutExerciseTypeNotFoundException {
        WorkoutExerciseType workoutExerciseType = workoutExerciseTypeRepository.findById(id);
        if (workoutExerciseType == null) {
            log.warn("No workout exercise type found by id: {} ", id);
            throw new WorkoutExerciseTypeNotFoundException(
                    LocaleKeys.WORKOUT_EXERCISE_TYPE_NOT_FOUND.getValue()
                            + " " + id);
        }
        log.info("{} found by id {}", workoutExerciseType, workoutExerciseType.getId());
        return workoutExerciseType;
    }

    @Override
    public Page<WorkoutExerciseType> getAll(Pageable pageable) {
        return workoutExerciseTypeRepository.findAll(pageable);
    }

    @Override
    public WorkoutExerciseType create(WorkoutExerciseTypeDto workoutExerciseTypeDto)
            throws WorkoutExerciseTypeExistException {
        WorkoutExerciseType workoutExerciseType = checkWorkoutExerciseType(workoutExerciseTypeDto.toWorkoutExerciseType());
        workoutExerciseType.setCreatedAt(new Date(System.currentTimeMillis()));
        workoutExerciseType.setUpdatedAt(new Date(System.currentTimeMillis()));
        return workoutExerciseTypeRepository.save(workoutExerciseType);
    }

    @Override
    public WorkoutExerciseType delete(UUID id) throws WorkoutExerciseTypeNotFoundException {
        WorkoutExerciseType workoutExerciseType = workoutExerciseTypeRepository.findById(id);
        if (workoutExerciseTypeRepository.findById(id) != null) {
            workoutExerciseTypeRepository.deleteById(id);
            log.info("Workout Exercise type with ID {} was deleted", id.toString());
        } else {
            log.warn("Workout Exercise type with ID {} not found", id.toString());
            throw new WorkoutExerciseTypeNotFoundException(
                    LocaleKeys.WORKOUT_EXERCISE_TYPE_NOT_FOUND.getValue()
                            + " " + id);
        }
        return workoutExerciseType;
    }

    @Override
    public void save(WorkoutExerciseType workoutExerciseType) {
        workoutExerciseTypeRepository.save(workoutExerciseType);
    }

    @Override
    public Map<String, List<String>> getExerciseTypeData() {
        Map<String, List<String>> exercisesData = new HashMap<>();
        exercisesData.put("format", getStringListValuesFromArray(Format.values()));
        exercisesData.put("wodPart", getStringListValuesFromArray(WodPart.values()));
        return exercisesData;
    }

    @Override
    public List<WorkoutExerciseTypeCsvDto> extractExerciseTypesFromCsv(MultipartFile file)
            throws IOException,
            WorkoutExerciseTypeExistException,
            CsvException,
            ExtensionFileException,
            EmptyFileException {
        List<WorkoutExerciseTypeCsvDto> exerciseTypesCsv;
        FileValidation.validateFile(file);
        Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                .withType(WorkoutExerciseTypeCsvDto.class)
                .withSeparator(';')
                .withIgnoreLeadingWhiteSpace(true)
                .withThrowExceptions(false)
                .build();
        exerciseTypesCsv = csvToBean.parse();
        FileValidation.validateFileLines(csvToBean);
        createExerciseTypesFromCsv(exerciseTypesCsv);
        return exerciseTypesCsv;
    }

    private void createExerciseTypesFromCsv(List<WorkoutExerciseTypeCsvDto> exerciseTypesFromCsv)
            throws WorkoutExerciseTypeExistException {
        for (WorkoutExerciseTypeCsvDto typeCsvDto : exerciseTypesFromCsv) {
            WorkoutExerciseType workoutExerciseType = checkWorkoutExerciseType(typeCsvDto.toWorkoutExerciseType());
            workoutExerciseType.setCreatedAt(new Date(System.currentTimeMillis()));
            workoutExerciseType.setUpdatedAt(new Date(System.currentTimeMillis()));
            workoutExerciseTypeRepository.save(workoutExerciseType);
        }
    }

    private WorkoutExerciseType checkWorkoutExerciseType(WorkoutExerciseType workoutExerciseType)
            throws WorkoutExerciseTypeExistException {
        WorkoutExerciseType uniqueWorkoutExerciseType =
                workoutExerciseTypeRepository.findUniqueWorkoutExerciseType(
                        workoutExerciseType.getWeight(),
                        workoutExerciseType.getSets(),
                        workoutExerciseType.getReps(),
                        workoutExerciseType.getTime(),
                        workoutExerciseType.getFormat()
                );
        if (uniqueWorkoutExerciseType != null) {
            log.error("Similar workout Exercise type already exists");
            throw new WorkoutExerciseTypeExistException(LocaleKeys.WORKOUT_EXERCISE_TYPE_EXIST.getValue());
        }
        return workoutExerciseType;
    }
}
