package com.crossfit.dto;

import com.crossfit.constants.Format;
import com.crossfit.constants.WodPart;
import com.crossfit.model.WorkoutExerciseType;
import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class WorkoutExerciseTypeCsvDto {

    @CsvBindByName
    private String weight;
    @CsvBindByName
    private int sets;
    @CsvBindByName
    private int reps;
    @CsvBindByName
    private int time;
    @CsvBindByName
    private String format;
    @CsvBindByName
    private String wodPart;

    public WorkoutExerciseType toWorkoutExerciseType() {
        WorkoutExerciseType workoutExerciseType = new WorkoutExerciseType();
        workoutExerciseType.setWeight(this.weight);
        workoutExerciseType.setSets(this.sets);
        workoutExerciseType.setReps(this.reps);
        workoutExerciseType.setTime(this.time);
        workoutExerciseType.setFormat(Format.valueOf(this.format));
        workoutExerciseType.setWodPart(WodPart.valueOf(this.wodPart));
        return workoutExerciseType;
    }
}
