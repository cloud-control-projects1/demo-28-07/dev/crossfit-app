package com.crossfit.dto;

import com.crossfit.constants.Format;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@Data
public class WorkoutOfDayView {

    private String name;
    private String description;
    private Set<WorkoutExercise> preWod;
    private Set<WorkoutExercise> wod;
    private Set<WorkoutExercise> postWod;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Format format;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rounds;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private long count;
    private int time;
}
