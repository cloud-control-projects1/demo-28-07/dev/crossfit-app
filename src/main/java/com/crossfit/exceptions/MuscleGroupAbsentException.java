package com.crossfit.exceptions;

public class MuscleGroupAbsentException extends Exception {
    public MuscleGroupAbsentException(String message) {
        super(message);
    }
}
