package com.crossfit.services.wodgenerators;

import com.crossfit.dto.WorkoutExercise;
import com.crossfit.model.Exercise;

import java.util.Set;
import java.util.stream.Stream;

public interface WodGenerator {

    Set<WorkoutExercise> buildSetOfWorkoutExercises(Stream<Exercise> exercises);
}
