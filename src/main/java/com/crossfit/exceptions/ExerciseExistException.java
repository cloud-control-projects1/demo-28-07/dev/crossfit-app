package com.crossfit.exceptions;

public class ExerciseExistException extends Exception {

    public ExerciseExistException(String message) {
        super(message);
    }
}
