package com.crossfit.services;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
    public static List<String> getStringListValuesFromArray(Object[] array) {
        return Arrays.stream(array).map(Object::toString).collect(Collectors.toList());
    }
}