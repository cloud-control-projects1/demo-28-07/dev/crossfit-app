package com.crossfit.dto;

import com.crossfit.model.Exercise;
import com.crossfit.model.WorkoutExerciseType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class WorkoutExercise {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<CrossfitExercise> crossfitExercises;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Exercise exercise;
    private WorkoutExerciseType workoutExerciseType;
}
