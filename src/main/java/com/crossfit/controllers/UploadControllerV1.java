package com.crossfit.controllers;

import com.crossfit.exceptions.EmptyFileException;
import com.crossfit.exceptions.ExtensionFileException;
import com.crossfit.exceptions.WorkoutExerciseTypeExistException;
import com.crossfit.services.ExerciseService;
import com.crossfit.services.WorkoutExerciseTypeService;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@RestController
@RequestMapping(value = "/api/v1/upload")
public class UploadControllerV1 {

    private final ExerciseService exerciseService;
    private final WorkoutExerciseTypeService workoutExerciseTypeService;

    @Autowired
    public UploadControllerV1(
            ExerciseService exerciseService,
            WorkoutExerciseTypeService workoutExerciseTypeService
    ) {
        this.exerciseService = exerciseService;
        this.workoutExerciseTypeService = workoutExerciseTypeService;
    }

    @PostMapping("exercise")
    public ResponseEntity<Object> uploadCSVFileWithExercises(@RequestParam("file") MultipartFile file) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(exerciseService.extractExercisesFromCsv(file));
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @PostMapping("workoutExerciseType")
    public ResponseEntity<Object> uploadCSVFileWithWorkoutExerciseTypes(@RequestParam("file") MultipartFile file)
            throws ExtensionFileException, WorkoutExerciseTypeExistException, EmptyFileException, IOException, CsvException {
        return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseTypeService.extractExerciseTypesFromCsv(file));
    }
}
