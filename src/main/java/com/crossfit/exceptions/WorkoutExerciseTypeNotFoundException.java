package com.crossfit.exceptions;

public class WorkoutExerciseTypeNotFoundException extends Exception {

    public WorkoutExerciseTypeNotFoundException(String message) {
        super(message);
    }
}
