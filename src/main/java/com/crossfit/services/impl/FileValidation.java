package com.crossfit.services.impl;

import com.crossfit.constants.LocaleKeys;
import com.crossfit.exceptions.EmptyFileException;
import com.crossfit.exceptions.ExtensionFileException;
import com.opencsv.bean.CsvToBean;
import com.opencsv.exceptions.CsvException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;

@Slf4j
public class FileValidation {

    public static void validateFile(MultipartFile file) throws EmptyFileException, ExtensionFileException {
        if (file.isEmpty()) {
            log.error("File is empty");
            throw new EmptyFileException(LocaleKeys.FILE_IS_EMPTY.getValue());
        }
        else if (!Objects.equals(FilenameUtils.getExtension(file.getOriginalFilename()), "csv")) {
            log.error("File extension is not csv");
            throw new ExtensionFileException(LocaleKeys.WRONG_FILE_EXTENSION.getValue() + ", is not csv");
        }
    }

    public static void validateFileLines(CsvToBean csvToBean) throws CsvException {
        List<CsvException> exceptions = csvToBean.getCapturedExceptions();
        log.warn("Number of mistakes: {}", exceptions.size());
        if (!exceptions.isEmpty()) {
            StringBuilder result = new StringBuilder();
            exceptions.forEach(e -> {
                result.append(e.getMessage());
                result.append("\n");
            });
            throw new CsvException(result.toString());
        }
    }
}
