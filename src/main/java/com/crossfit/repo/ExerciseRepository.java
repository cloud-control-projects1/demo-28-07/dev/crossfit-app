package com.crossfit.repo;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import com.crossfit.model.Exercise;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ExerciseRepository extends JpaRepository<Exercise, String> {

    Page<Exercise> findAll(Pageable pageable);

    Exercise findById(UUID id);

    Exercise findByName(String name);

    void deleteExerciseById(UUID id);

    List<Exercise> getAllByNameContaining(String name);

    List<Exercise> getAllByDescriptionContaining(String description);

    @Query("select distinct level from Exercise")
    List<String> getLevels();

    @Query("select distinct muscleGroup from Exercise")
    List<String> getMuscleGroup();

    @Query("select distinct type from Exercise")
    List<String> getType();

    @Query("select distinct wodPart from Exercise")
    List<String> getWodPart();

    @Query("select e from Exercise e where e.wodPart = ?1 and e.muscleGroup = ?2")
    List<Exercise> getByWodPartAndMuscleGroup(WodPart wodPart, MuscleGroup muscleGroup);

    @Query("select e from Exercise e where e.wodPart = ?1 and e.muscleGroup = ?2 and e.level = 'BEGINNER'")
    List<Exercise> getByWodPartForBeginner(WodPart wodPart, MuscleGroup muscleGroup);

    @Query("select e from Exercise e where e.wodPart = ?1 and e.muscleGroup = ?2 and not e.level = 'PRO'")
    List<Exercise> getByWodPartForAmateur(WodPart wodPart, MuscleGroup muscleGroup);

    @Query("select e from Exercise e where e.wodPart = ?1")
    List<Exercise> getByWodPartAndMuscleGroupForCrossfit(WodPart wodPart);

    @Query("select e from Exercise e where e.wodPart = ?1 and e.level = 'BEGINNER'")
    List<Exercise> getByWodPartForBeginnerForCrossfit(WodPart wodPart);

    @Query("select e from Exercise e where e.wodPart = ?1 and not e.level = 'PRO'")
    List<Exercise> getByWodPartForAmateurForCrossfit(WodPart wodPart);

    List<Exercise> findByMuscleGroup(MuscleGroup muscleGroup);

    List<Exercise> findByLevel(Level level);

    List<Exercise> findByType(Type type);

    List<Exercise> findByWodPart(WodPart wodPart);
}
