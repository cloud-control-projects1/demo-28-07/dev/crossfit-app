package com.crossfit.services;

import com.crossfit.dto.WorkoutExerciseTypeCsvDto;
import com.crossfit.dto.WorkoutExerciseTypeDto;
import com.crossfit.exceptions.EmptyFileException;
import com.crossfit.exceptions.ExtensionFileException;
import com.crossfit.exceptions.WorkoutExerciseTypeExistException;
import com.crossfit.exceptions.WorkoutExerciseTypeNotFoundException;
import com.crossfit.model.WorkoutExerciseType;
import com.opencsv.exceptions.CsvException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface WorkoutExerciseTypeService {

    WorkoutExerciseType getByID(UUID id) throws WorkoutExerciseTypeNotFoundException;

    Page<WorkoutExerciseType> getAll(Pageable pageable);

    WorkoutExerciseType create(WorkoutExerciseTypeDto workoutExerciseTypeDto) throws WorkoutExerciseTypeExistException;

    WorkoutExerciseType delete(UUID id) throws WorkoutExerciseTypeNotFoundException;

    void save(WorkoutExerciseType workoutExerciseType);

    Map getExerciseTypeData();

    List<WorkoutExerciseTypeCsvDto> extractExerciseTypesFromCsv(MultipartFile file) throws IOException, WorkoutExerciseTypeExistException, CsvException, ExtensionFileException, EmptyFileException;
}
