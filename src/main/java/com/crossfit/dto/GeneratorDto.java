package com.crossfit.dto;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.WorkoutOfDayType;
import lombok.Data;

@Data
public class GeneratorDto {
    private Level level;
    private WorkoutOfDayType workoutOfDayType;
    private MuscleGroup muscleGroup;
    private int metconCrossfitCount;
}
