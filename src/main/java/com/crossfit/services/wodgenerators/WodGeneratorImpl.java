package com.crossfit.services.wodgenerators;

import com.crossfit.constants.Format;
import com.crossfit.dto.WorkoutExercise;
import com.crossfit.model.Exercise;
import com.crossfit.model.WorkoutExerciseType;
import com.crossfit.repo.WorkoutExerciseTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.crossfit.services.RandomizerForExercises.randomizeWorkoutExerciseType;

@Component
public class WodGeneratorImpl implements WodGenerator {

    private final WorkoutExerciseTypeRepository workoutExerciseTypeRepository;

    @Autowired
    public WodGeneratorImpl(WorkoutExerciseTypeRepository workoutExerciseTypeRepository) {
        this.workoutExerciseTypeRepository = workoutExerciseTypeRepository;
    }

    @Override
    public Set<WorkoutExercise> buildSetOfWorkoutExercises(Stream<Exercise> exercises) {
        return exercises.map(exercise -> {
            List<WorkoutExerciseType> exerciseTypes;
            WorkoutExerciseType randomType;
            exerciseTypes = workoutExerciseTypeRepository
                    .getByWodPartAndFormat(exercise.getWodPart(), Format.DEFAULT);
            randomType = randomizeWorkoutExerciseType(exerciseTypes);
            return WorkoutExercise
                    .builder()
                    .workoutExerciseType(randomType)
                    .exercise(exercise)
                    .build();
        }).collect(Collectors.toSet());
    }
}
