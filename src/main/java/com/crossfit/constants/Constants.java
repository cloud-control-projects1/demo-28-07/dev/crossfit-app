package com.crossfit.constants;

public class Constants {

    public static final String GYM_NAME = "Gym workout";
    public static final String CROSSFIT_NAME = "Crossfit";
    public static final String FITNESS_NAME = "Fitness";

    public static final int EXERCISES_COUNT_PRE_WOD = 3;
    public static final int EXERCISES_COUNT_WOD = 5;
    public static final int EXERCISES_COUNT_POST_WOD = 2;

    public static final int[] AMRAP_REPS_COUNT = {3, 5, 8, 10, 15};
    public static final int[] AFAP_REPS_COUNT = {5, 10, 15, 20, 25};
    public static final int[] EMOM_REPS_COUNT = {3, 5, 8, 10};
    public static final int[] CHIPPER_REPS_COUNT = {10, 20, 30, 40, 50};
}
