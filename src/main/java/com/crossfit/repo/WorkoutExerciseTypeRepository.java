package com.crossfit.repo;

import com.crossfit.constants.Format;
import com.crossfit.constants.WodPart;
import com.crossfit.model.WorkoutExerciseType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface WorkoutExerciseTypeRepository extends JpaRepository<WorkoutExerciseType, String> {

    Page<WorkoutExerciseType> findAll(Pageable pageable);

    WorkoutExerciseType findById(UUID id);

    void deleteById(UUID id);

    WorkoutExerciseType findByWeight(String weight);

    WorkoutExerciseType findBySets(int sets);

    WorkoutExerciseType findByReps(int reps);

    WorkoutExerciseType findByTime(int time);

    List<WorkoutExerciseType> findByWodPart(WodPart wodPart);

    @Query("select e from WorkoutExerciseType e where e.wodPart = ?1 and e.format = ?2")
    List<WorkoutExerciseType> getByWodPartAndFormat(WodPart wodPart, Format format);

    @Query("select e from WorkoutExerciseType e where e.wodPart = ?1 and not e.format = 'DEFAULT'")
    List<WorkoutExerciseType> getByWodPartAndFormatForCrossfit(WodPart wodPart);

    @Query("select e from WorkoutExerciseType e where " +
            " e.weight = ?1 and e.sets = ?2 and" +
            " e.reps = ?3 and e.time = ?4 and" +
            " e.format = ?5")
    WorkoutExerciseType findUniqueWorkoutExerciseType(String weight, int sets, int reps, int time, Format format);
}
