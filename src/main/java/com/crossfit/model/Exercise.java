package com.crossfit.model;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "exercise")
@Data
public class Exercise extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private Level level;
    @Enumerated(EnumType.STRING)
    private Type type;
    @Enumerated(EnumType.STRING)
    private MuscleGroup muscleGroup;
    @Enumerated(EnumType.STRING)
    private WodPart wodPart;

    public Exercise() {
    }

    @Override
    public String toString() {
        return name;
    }
}
