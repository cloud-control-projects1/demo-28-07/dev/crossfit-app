package com.crossfit.constants;

public enum WodPart {
    PRE_WOD, WOD, POST_WOD
}
