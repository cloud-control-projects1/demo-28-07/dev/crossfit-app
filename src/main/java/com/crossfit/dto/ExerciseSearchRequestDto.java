package com.crossfit.dto;

import lombok.Data;

@Data
public class ExerciseSearchRequestDto {
    private String term;
    private ExerciseFilterDto filters;
}
