package com.crossfit.constants;

public enum Format {
    EMOM, AMRAP, AFAP, TABATA, CHIPPER, DEFAULT
}
