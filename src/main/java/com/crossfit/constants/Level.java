package com.crossfit.constants;

public enum Level {
    BEGINNER, AMATEUR, PRO
}
