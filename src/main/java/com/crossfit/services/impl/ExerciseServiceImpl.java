package com.crossfit.services.impl;

import com.crossfit.constants.*;
import com.crossfit.dto.ExerciseCsvDto;
import com.crossfit.dto.ExerciseDto;
import com.crossfit.exceptions.*;
import com.crossfit.model.Exercise;
import com.crossfit.repo.ExerciseRepository;
import com.crossfit.services.ExerciseService;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.crossfit.services.Utils.getStringListValuesFromArray;

@Service
@Slf4j
public class ExerciseServiceImpl implements ExerciseService {

    private final ExerciseRepository exerciseRepository;

    @Autowired
    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public Set<Exercise> getAllByTermContaining(String term) {
        List<Exercise> exercisesById = getExercisesById(term);
        List<Exercise> exercisesByName = exerciseRepository
                .getAllByNameContaining(term);
        List<Exercise> exercisesByDescription = exerciseRepository
                .getAllByDescriptionContaining(term);
        return Stream.of(exercisesById, exercisesByName, exercisesByDescription)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public Page<Exercise> getAll(Pageable pageable) {
        return exerciseRepository.findAll(pageable);
    }

    @Override
    public Exercise getById(UUID id) throws ExerciseNotFoundException {
        Exercise exerciseById = exerciseRepository.findById(id);
        if (exerciseById == null) {
            log.warn("No exercise found by id: {} ", id);
            throw new ExerciseNotFoundException(LocaleKeys.EXERCISE_NOT_FOUND.getValue() + " " + id);
        }
        log.info("{} found by id {}", exerciseById.getName(), exerciseById.getId());
        return exerciseById;
    }

    @Override
    public void save(Exercise exercise) {
        exerciseRepository.save(exercise);
    }

    @Override
    public Exercise delete(UUID id) throws ExerciseNotFoundException {
        Exercise exercise = exerciseRepository.findById(id);
        if (exerciseRepository.findById(id) != null) {
            exerciseRepository.deleteExerciseById(id);
            log.info("Exercise with ID {} was deleted", id.toString());
        } else {
            log.warn("Exercise with ID {} not found", id.toString());
            throw new ExerciseNotFoundException(LocaleKeys.EXERCISE_NOT_FOUND.getValue() + " " + id);
        }
        return exercise;
    }

    @Override
    public Exercise create(ExerciseDto exerciseDto) throws
            MuscleGroupAbsentException,
            ExerciseExistException,
            TypeAbsentException,
            ExerciseNameAbsentException,
            LevelAbsentException {
        Exercise exercise = checkExercise(exerciseDto);
        exercise.setCreatedAt(new Date(System.currentTimeMillis()));
        exercise.setUpdatedAt(new Date(System.currentTimeMillis()));
        return exerciseRepository.save(exercise);
    }

    @Override
    public Map<String, List<String>> getExerciseData() {
        Map<String, List<String>> exercisesData = new HashMap<>();
        exercisesData.put("level", getStringListValuesFromArray(Level.values()));
        exercisesData.put("group", getStringListValuesFromArray(MuscleGroup.values()));
        exercisesData.put("type", getStringListValuesFromArray(Type.values()));
        exercisesData.put("wodPart", getStringListValuesFromArray(WodPart.values()));
        return exercisesData;
    }

    @Override
    public List<ExerciseCsvDto> extractExercisesFromCsv(MultipartFile file)
            throws ExtensionFileException,
            EmptyFileException,
            IOException,
            CsvException,
            ExerciseExistException,
            ExerciseNameAbsentException {
        List<ExerciseCsvDto> exercisesCsv;
        FileValidation.validateFile(file);
        Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                .withType(ExerciseCsvDto.class)
                .withSeparator(';')
                .withIgnoreLeadingWhiteSpace(true)
                .withThrowExceptions(false)
                .build();
        exercisesCsv = csvToBean.parse();
        FileValidation.validateFileLines(csvToBean);
        createExercisesFromCsv(exercisesCsv);
        return exercisesCsv;
    }

    private void createExercisesFromCsv(List<ExerciseCsvDto> exercisesFromCsv) throws
            ExerciseExistException,
            ExerciseNameAbsentException {
        for (ExerciseCsvDto exerciseCsv : exercisesFromCsv) {
            checkExerciseName(exerciseCsv.getName());
            Exercise exercise = exerciseCsv.toExercise();
            exercise.setCreatedAt(new Date(System.currentTimeMillis()));
            exercise.setUpdatedAt(new Date(System.currentTimeMillis()));
            exerciseRepository.save(exercise);
        }
    }

    private Exercise checkExercise(ExerciseDto exerciseDto) throws
            ExerciseExistException,
            ExerciseNameAbsentException,
            LevelAbsentException,
            TypeAbsentException,
            MuscleGroupAbsentException {
        Level level = exerciseDto.getLevel();
        Type type = exerciseDto.getType();
        MuscleGroup muscleGroup = exerciseDto.getMuscleGroup();
        WodPart wodPart = exerciseDto.getWodPart();
        checkExerciseName(exerciseDto.getName());
        if (level == null) {
            log.error("Level is null");
            throw new LevelAbsentException(LocaleKeys.LEVEL_IS_NULL.getValue());
        }
        if (type == null) {
            log.error("Type is null");
            throw new TypeAbsentException(LocaleKeys.TYPE_IS_NULL.getValue());
        }
        if (muscleGroup == null) {
            log.error("Muscle Group is null");
            throw new MuscleGroupAbsentException(LocaleKeys.MUSCLE_GROUP_IS_NULL.getValue());
        }
        if (wodPart == null) {
            log.error("Wod Part is null");
            throw new MuscleGroupAbsentException(LocaleKeys.WOD_PART_IS_NULL.getValue());
        }
        return exerciseDto.toExercise();
    }

    private void checkExerciseName(String name) throws
            ExerciseExistException,
            ExerciseNameAbsentException {
        if (exerciseRepository.findByName(name) != null) {
            log.error("Exercise already exists");
            throw new ExerciseExistException(LocaleKeys.EXERCISE_EXIST.getValue());
        }
        if (name == null) {
            log.error("Exercise name is null");
            throw new ExerciseNameAbsentException(LocaleKeys.EXERCISE_NAME_IS_NULL.getValue());
        }
    }

    private List<Exercise> getExercisesById(String id) {
        List<Exercise> exercisesById;
        try {
            exercisesById = List.of(exerciseRepository.findById(UUID.fromString(id)));
        } catch (Exception exception) {
            exercisesById = new ArrayList<>();
        }
        return exercisesById;
    }
}
