package com.crossfit.services;

import com.crossfit.dto.ExerciseCsvDto;
import com.crossfit.dto.ExerciseDto;
import com.crossfit.exceptions.*;
import com.crossfit.model.Exercise;
import com.opencsv.exceptions.CsvException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public interface ExerciseService {

    Set<Exercise> getAllByTermContaining(String term);

    Page<Exercise> getAll(Pageable pageable);

    Exercise getById(UUID id) throws ExerciseNotFoundException;

    void save(Exercise exercise);

    Exercise delete(UUID id) throws ExerciseNotFoundException;

    Exercise create(ExerciseDto exerciseDto) throws
            MuscleGroupAbsentException,
            ExerciseExistException,
            TypeAbsentException,
            ExerciseNameAbsentException,
            LevelAbsentException;

    Map<String, List<String>> getExerciseData();

    List<ExerciseCsvDto> extractExercisesFromCsv(MultipartFile file) throws ExtensionFileException, EmptyFileException, IOException, CsvException, ExerciseExistException, ExerciseNameAbsentException;
}
