package com.crossfit.dto;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import com.crossfit.model.Exercise;
import lombok.Data;

@Data
public class ExerciseDto {
    private String name;
    private String description;
    private Level level;
    private Type type;
    private MuscleGroup muscleGroup;
    private WodPart wodPart;

    public Exercise toExercise() {
        Exercise exercise = new Exercise();
        exercise.setName(this.name);
        exercise.setDescription(this.description);
        exercise.setLevel(this.level);
        exercise.setType(this.type);
        exercise.setMuscleGroup(this.muscleGroup);
        exercise.setWodPart(this.wodPart);
        return exercise;
    }
}
