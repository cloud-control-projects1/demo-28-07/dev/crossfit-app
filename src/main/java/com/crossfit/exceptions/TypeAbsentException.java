package com.crossfit.exceptions;

public class TypeAbsentException extends Exception {
    public TypeAbsentException(String message) {
        super(message);
    }
}
