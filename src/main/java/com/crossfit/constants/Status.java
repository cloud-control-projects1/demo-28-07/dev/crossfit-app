package com.crossfit.constants;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
