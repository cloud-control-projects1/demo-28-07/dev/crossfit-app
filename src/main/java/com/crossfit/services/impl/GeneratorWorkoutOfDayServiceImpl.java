package com.crossfit.services.impl;

import com.crossfit.constants.*;
import com.crossfit.dto.GeneratorDto;
import com.crossfit.dto.WorkoutExercise;
import com.crossfit.dto.WorkoutOfDayView;
import com.crossfit.model.Exercise;
import com.crossfit.repo.ExerciseRepository;
import com.crossfit.services.wodgenerators.CrossfitWodGenerator;
import com.crossfit.services.GeneratorWorkoutOfDayService;
import com.crossfit.services.wodgenerators.WodGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static com.crossfit.constants.Constants.*;
import static com.crossfit.services.RandomizerForExercises.randomizeExercises;

@Service
public class GeneratorWorkoutOfDayServiceImpl implements GeneratorWorkoutOfDayService {

    private final ExerciseRepository exerciseRepository;
    private final CrossfitWodGenerator crossfitWodGenerator;
    private final WodGenerator wodGenerator;

    @Autowired
    public GeneratorWorkoutOfDayServiceImpl(
            ExerciseRepository exerciseRepository,
            CrossfitWodGenerator crossfitWodGenerator,
            WodGenerator wodGenerator) {
        this.exerciseRepository = exerciseRepository;
        this.crossfitWodGenerator = crossfitWodGenerator;
        this.wodGenerator = wodGenerator;
    }

    @Override
    public WorkoutOfDayView generateWorkout(GeneratorDto generatorDto) {
        MuscleGroup muscleGroup = generatorDto.getMuscleGroup();
        Level level = generatorDto.getLevel();
        WorkoutOfDayType workoutOfDayType = generatorDto.getWorkoutOfDayType();
        int metconCrossfitCount = generatorDto.getMetconCrossfitCount();

        WorkoutOfDayView workoutOfDayView = new WorkoutOfDayView();

        Set<WorkoutExercise> preWod = createPreWod(muscleGroup, workoutOfDayType);
        Set<WorkoutExercise> wod = createWod(muscleGroup, level, workoutOfDayType, metconCrossfitCount);
        Set<WorkoutExercise> postWod = createPostWod(muscleGroup, workoutOfDayType);

        workoutOfDayView.setPreWod(preWod);
        workoutOfDayView.setWod(wod);
        workoutOfDayView.setPostWod(postWod);

        switch (workoutOfDayType) {
            case GYM:
                workoutOfDayView.setName(GYM_NAME);
                workoutOfDayView.setFormat(Format.DEFAULT);
                break;
            case CROSSFIT:
                workoutOfDayView.setName(CROSSFIT_NAME);
                break;
            case FITNESS:
                workoutOfDayView.setName(FITNESS_NAME);
                workoutOfDayView.setFormat(Format.DEFAULT);
                break;
        }
        workoutOfDayView.setCount(Objects.requireNonNull(wod).size());

        return workoutOfDayView;
    }

    private Set<WorkoutExercise> createPreWod(MuscleGroup muscleGroup, WorkoutOfDayType workoutOfDayType) {
        List<Exercise> preWodExercises;
        Stream<Exercise> randomExercises;
        switch (workoutOfDayType) {
            case GYM:
                preWodExercises = exerciseRepository
                        .getByWodPartAndMuscleGroup(WodPart.PRE_WOD, muscleGroup);
                randomExercises = randomizeExercises(EXERCISES_COUNT_PRE_WOD, preWodExercises);
                return wodGenerator.buildSetOfWorkoutExercises(randomExercises);
            case CROSSFIT:
                preWodExercises = exerciseRepository
                        .getByWodPartAndMuscleGroupForCrossfit(WodPart.PRE_WOD);
                randomExercises = randomizeExercises(EXERCISES_COUNT_PRE_WOD, preWodExercises);
                return wodGenerator.buildSetOfWorkoutExercises(randomExercises);
            case FITNESS:
            default:
                return null;
        }
    }

    private Set<WorkoutExercise> createWod(
            MuscleGroup muscleGroup,
            Level level,
            WorkoutOfDayType workoutOfDayType,
            int metconCrossfitCount) {
        List<Exercise> wodExercises;
        Stream<Exercise> randomExercises;
        switch (workoutOfDayType) {
            case GYM:
                wodExercises = getWodExercises(muscleGroup, level);
                randomExercises = randomizeExercises(EXERCISES_COUNT_WOD,
                        Objects.requireNonNull(wodExercises));
                return wodGenerator.buildSetOfWorkoutExercises(randomExercises);
            case CROSSFIT:
                return crossfitWodGenerator
                        .buildWorkoutExerciseForCrossfit(metconCrossfitCount, level);
            case FITNESS:
            default:
                return null;
        }
    }

    private List<Exercise> getWodExercises(MuscleGroup muscleGroup, Level level) {
        switch (level) {
            case BEGINNER:
                return exerciseRepository.getByWodPartForBeginner(WodPart.WOD, muscleGroup);
            case AMATEUR:
                return exerciseRepository.getByWodPartForAmateur(WodPart.WOD, muscleGroup);
            case PRO:
                return exerciseRepository.getByWodPartAndMuscleGroup(WodPart.WOD, muscleGroup);
            default:
                return null;
        }
    }

    private Set<WorkoutExercise> createPostWod(MuscleGroup muscleGroup, WorkoutOfDayType workoutOfDayType) {
        List<Exercise> postWodExercises;
        Stream<Exercise> randomExercises;
        switch (workoutOfDayType) {
            case GYM:
                postWodExercises = exerciseRepository
                        .getByWodPartAndMuscleGroup(WodPart.POST_WOD, muscleGroup);
                randomExercises = randomizeExercises(EXERCISES_COUNT_POST_WOD, postWodExercises);
                return wodGenerator.buildSetOfWorkoutExercises(randomExercises);
            case CROSSFIT:
                postWodExercises = exerciseRepository
                        .getByWodPartAndMuscleGroupForCrossfit(WodPart.POST_WOD);
                randomExercises = randomizeExercises(EXERCISES_COUNT_POST_WOD, postWodExercises);
                return wodGenerator.buildSetOfWorkoutExercises(randomExercises);
            case FITNESS:
            default:
                return null;
        }
    }
}
