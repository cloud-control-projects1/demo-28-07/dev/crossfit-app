package com.crossfit.constants;

public enum LocaleKeys {

    EXERCISE_EXIST("Exercise with this name already exists"),
    EXERCISE_NOT_FOUND("Exercise not found"),
    EXERCISE_NAME_IS_NULL("Exercise name is null"),
    LEVEL_IS_NULL("Level is null"),
    TYPE_IS_NULL("Type is null"),
    MUSCLE_GROUP_IS_NULL("Muscle Group is null"),
    WOD_PART_IS_NULL("Wod Part is null"),
    EXERCISE_CREATE_SUCCESS("Exercise created successfully"),

    WORKOUT_EXERCISE_TYPE_NOT_FOUND("Workout Exercise type not found"),
    WORKOUT_EXERCISE_TYPE_EXIST("Similar Workout Exercise type already exists"),

    FILE_IS_EMPTY("File is empty"),
    WRONG_FILE_EXTENSION("Wrong file extension");

    private final String value;

    LocaleKeys(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
