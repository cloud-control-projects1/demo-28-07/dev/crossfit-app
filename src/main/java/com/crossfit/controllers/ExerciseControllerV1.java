package com.crossfit.controllers;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import com.crossfit.dto.ExerciseDto;
import com.crossfit.dto.ExerciseSearchRequestDto;
import com.crossfit.model.Exercise;
import com.crossfit.services.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@RestController
@RequestMapping(value = "/api/v1/exercise")
public class ExerciseControllerV1 {

    private final ExerciseService exerciseService;

    @Autowired
    public ExerciseControllerV1(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @PostMapping("/search")
    public ResponseEntity<Object> searchExercise(
            @RequestBody ExerciseSearchRequestDto exercisePageRequestDto,
            @PageableDefault(size = 40, sort = {"updatedAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        try {
            Page<Exercise> exercises = exerciseService.getAll(pageable);
            List<Level> levels = exercisePageRequestDto.getFilters().getLevels();
            List<Type> types = exercisePageRequestDto.getFilters().getTypes();
            List<MuscleGroup> muscleGroups = exercisePageRequestDto.getFilters().getMuscleGroups();
            List<WodPart> wodParts = exercisePageRequestDto.getFilters().getWodParts();

            if (exercisePageRequestDto.getTerm() != null) {
                exercises = new PageImpl<>(new ArrayList<>(exerciseService
                        .getAllByTermContaining(exercisePageRequestDto.getTerm())));
            }
            if (levels != null && !levels.isEmpty()) {
                exercises = new PageImpl<>(exercises.stream()
                        .filter(exercise -> levels.contains(exercise.getLevel()))
                        .collect(Collectors.toList()), exercises.getPageable(), exercises.getSize());
            }
            if (types != null && !types.isEmpty()) {
                exercises = new PageImpl<>(exercises.stream()
                        .filter(exercise -> types.contains(exercise.getType()))
                        .collect(Collectors.toList()), exercises.getPageable(), exercises.getSize());
            }
            if (muscleGroups != null && !muscleGroups.isEmpty()) {
                exercises = new PageImpl<>(exercises.stream()
                        .filter(exercise -> muscleGroups.contains(exercise.getMuscleGroup()))
                        .collect(Collectors.toList()), exercises.getPageable(), exercises.getSize());
            }
            if (wodParts != null && !wodParts.isEmpty()) {
                exercises = new PageImpl<>(exercises.stream()
                        .filter(exercise -> wodParts.contains(exercise.getWodPart()))
                        .collect(Collectors.toList()), exercises.getPageable(), exercises.getSize());
            }

            return ResponseEntity.status(HttpStatus.OK).body(exercises);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @GetMapping("/data")
    public ResponseEntity<Object> getExercisesData() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(exerciseService.getExerciseData());
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @GetMapping("/{exerciseId}")
    public ResponseEntity<Object> getExerciseById(@PathVariable UUID exerciseId) {
        try {
            Exercise exercise = exerciseService.getById(exerciseId);
            return ResponseEntity.status(HttpStatus.OK).body(exercise);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Object> createExercise(@RequestBody ExerciseDto exerciseDto) {
        try {
            Exercise exercise = exerciseService.create(exerciseDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(exercise);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @PutMapping("/{exerciseId}")
    public ResponseEntity<Object> updateExercise(
            @RequestBody ExerciseDto exerciseDto,
            @PathVariable UUID exerciseId) {
        try {
            Exercise currentExercise = exerciseService.getById(exerciseId);
            currentExercise.setName(exerciseDto.getName());
            currentExercise.setDescription(exerciseDto.getDescription());
            currentExercise.setLevel(exerciseDto.getLevel());
            currentExercise.setType(exerciseDto.getType());
            currentExercise.setMuscleGroup(exerciseDto.getMuscleGroup());
            currentExercise.setWodPart(exerciseDto.getWodPart());
            currentExercise.setUpdatedAt(new Date(System.currentTimeMillis()));
            exerciseService.save(currentExercise);
            return ResponseEntity.status(HttpStatus.OK).body(currentExercise);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @Transactional
    @DeleteMapping("/{exerciseId}")
    public ResponseEntity<Object> deleteExercise(@PathVariable UUID exerciseId) {
        try {
            Exercise exercise = exerciseService.delete(exerciseId);
            return ResponseEntity.status(HttpStatus.OK).body(exercise);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }
}
