
package com.crossfit.exceptions;

public class ExerciseNotFoundException extends Exception {
    public ExerciseNotFoundException(String message) {
        super(message);
    }
}
