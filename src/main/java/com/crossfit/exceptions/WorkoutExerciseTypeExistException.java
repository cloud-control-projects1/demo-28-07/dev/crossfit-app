package com.crossfit.exceptions;

public class WorkoutExerciseTypeExistException extends Exception {

    public WorkoutExerciseTypeExistException(String message) {
        super(message);
    }
}
