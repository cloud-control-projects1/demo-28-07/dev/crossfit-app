package com.crossfit.controllers;

import com.crossfit.dto.WorkoutExerciseTypeDto;
import com.crossfit.exceptions.WorkoutExerciseTypeExistException;
import com.crossfit.model.WorkoutExerciseType;
import com.crossfit.services.WorkoutExerciseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.UUID;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@RestController
@RequestMapping(value = "/api/v1/workoutexercisetype")
public class WorkoutExerciseTypeControllerV1 {

    private final WorkoutExerciseTypeService workoutExerciseTypeService;

    @Autowired
    public WorkoutExerciseTypeControllerV1(WorkoutExerciseTypeService workoutExerciseTypeService) {
        this.workoutExerciseTypeService = workoutExerciseTypeService;
    }

    @GetMapping("/data")
    public ResponseEntity<Object> getWorkoutExerciseTypesData() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseTypeService.getExerciseTypeData());
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<Object> getWorkoutExerciseTypes(
            @PageableDefault(size = 40, sort = {"updatedAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        try {
            Page<WorkoutExerciseType> workoutExerciseTypes = workoutExerciseTypeService.getAll(pageable);
            return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseTypes);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @GetMapping("/{workoutExerciseTypeId}")
    public ResponseEntity<Object> getWorkoutExerciseTypeById(@PathVariable UUID workoutExerciseTypeId) {
        try {
            WorkoutExerciseType workoutExerciseType = workoutExerciseTypeService.getByID(workoutExerciseTypeId);
            return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseType);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Object> createWorkoutExerciseType(@RequestBody WorkoutExerciseTypeDto workoutExerciseTypeDto)
            throws WorkoutExerciseTypeExistException {
        WorkoutExerciseType workoutExerciseType = workoutExerciseTypeService.create(workoutExerciseTypeDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(workoutExerciseType);
    }

    @PutMapping("/{workoutExerciseTypeId}")
    public ResponseEntity<Object> updateWorkoutExerciseType(
            @RequestBody WorkoutExerciseTypeDto workoutExerciseTypeDto,
            @PathVariable UUID workoutExerciseTypeId) {
        try {
            WorkoutExerciseType workoutExerciseType = workoutExerciseTypeService
                    .getByID(workoutExerciseTypeId);
            workoutExerciseType.setFormat(workoutExerciseTypeDto.getFormat());
            workoutExerciseType.setWodPart(workoutExerciseTypeDto.getWodPart());
            workoutExerciseType.setWeight(workoutExerciseTypeDto.getWeight());
            workoutExerciseType.setSets(workoutExerciseTypeDto.getSets());
            workoutExerciseType.setReps(workoutExerciseTypeDto.getReps());
            workoutExerciseType.setTime(workoutExerciseTypeDto.getTime());
            workoutExerciseType.setUpdatedAt(new Date(System.currentTimeMillis()));
            workoutExerciseTypeService.save(workoutExerciseType);
            return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseType);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

    @Transactional
    @DeleteMapping("/{exerciseId}")
    public ResponseEntity<Object> deleteExercise(@PathVariable UUID exerciseId) {
        try {
            WorkoutExerciseType workoutExerciseType = workoutExerciseTypeService.delete(exerciseId);
            return ResponseEntity.status(HttpStatus.OK).body(workoutExerciseType);
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }
}
