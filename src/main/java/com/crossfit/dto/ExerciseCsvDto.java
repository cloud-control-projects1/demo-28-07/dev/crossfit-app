package com.crossfit.dto;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import com.crossfit.model.Exercise;
import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class ExerciseCsvDto {

    @CsvBindByName
    private String description;
    @CsvBindByName
    private String level;
    @CsvBindByName
    private String muscleGroup;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private String type;
    @CsvBindByName
    private String wodPart;

    public Exercise toExercise() throws IllegalArgumentException {
        Exercise exercise = new Exercise();
        exercise.setName(this.name);
        exercise.setDescription(this.description);
        exercise.setLevel(Level.valueOf(this.level));
        exercise.setType(Type.valueOf(this.type));
        exercise.setMuscleGroup(MuscleGroup.valueOf(this.muscleGroup));
        exercise.setWodPart(WodPart.valueOf(this.wodPart));
        return exercise;
    }
}
