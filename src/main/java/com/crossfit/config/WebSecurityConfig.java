package com.crossfit.config;

import com.crossfit.security.jwt.JwtConfigurer;
import com.crossfit.security.jwt.JwtTokenProvider;
import com.crossfit.security.oauth.CustomOAuth2UserService;
import com.crossfit.security.oauth.CustomOauth2SuccessHandler;
import com.crossfit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

///oauth2/authorization/google
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final CustomOAuth2UserService oauthUserService;

    private static final String LOGIN_ENDPOINT = "/api/v1/auth/login";
    private static final String OAUTH_ENDPOINT = "/oauth2/**";

    @Autowired
    public WebSecurityConfig(JwtTokenProvider jwtTokenProvider,
                             @Lazy UserService userService,
                             CustomOAuth2UserService oauthUserService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.oauthUserService = oauthUserService;
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(OAUTH_ENDPOINT).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider))
                .and()
                .oauth2Login()
                .userInfoEndpoint()
                .userService(oauthUserService)
                .and()
                .successHandler(successHandler());
    }

    @Bean
    public CustomOauth2SuccessHandler successHandler() {
        return new CustomOauth2SuccessHandler(userService);
    }
}
