package com.crossfit.services.wodgenerators;

import com.crossfit.constants.Level;
import com.crossfit.dto.WorkoutExercise;

import java.util.Set;

public interface CrossfitWodGenerator {

    Set<WorkoutExercise> buildWorkoutExerciseForCrossfit(int metconCrossfitCount, Level level);
}
