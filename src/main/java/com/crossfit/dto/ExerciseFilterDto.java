package com.crossfit.dto;

import com.crossfit.constants.Level;
import com.crossfit.constants.MuscleGroup;
import com.crossfit.constants.Type;
import com.crossfit.constants.WodPart;
import lombok.Data;

import java.util.List;

@Data
public class ExerciseFilterDto {
    private List<Level> levels;
    private List<Type> types;
    private List<MuscleGroup> muscleGroups;
    private List<WodPart> wodParts;
}
