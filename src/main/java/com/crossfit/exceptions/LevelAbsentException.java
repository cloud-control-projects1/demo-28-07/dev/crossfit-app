package com.crossfit.exceptions;

public class LevelAbsentException extends Exception {
    public LevelAbsentException(String message) {
        super(message);
    }
}
