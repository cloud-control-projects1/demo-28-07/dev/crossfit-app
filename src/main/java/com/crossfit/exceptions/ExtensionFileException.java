package com.crossfit.exceptions;

public class ExtensionFileException extends Exception {
    public ExtensionFileException(String message) {
        super(message);
    }
}
